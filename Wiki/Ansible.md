# Ansible et CLI
Voici un petit tutoriel afin de démarrer rapidement avec Ansible et OpenStack du CROcc (Cloud Régional Occitanie).

sources :
- https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
- https://galaxy.ansible.com/openstack/cloud
- https://docs.ansible.com/ansible/latest/collections/openstack/cloud/index.html#plugins-in-openstack-cloud

## A - Installation Ansible
Tout d'abord nous allons installer l'environnement Ansible.

### A.1 - Installation
Il existe beaucoup de façon d'installer Ansible. Nous allons ici aborder la façon avec pip et un environnement virtuel python. Pour cela, il faut que python soit installé (de préférence python3), ainsi que le package venv de python :

        $ sudo apt -y install python3 python3-venv

CRéez ensuite un environnement virtuel :

       $ python3 -m venv ansible-crocc

Activez cette environnement virtuel :

:warning:l'activation de l'environnement virtuel doit se faire à chaque nouvelle connexion

        $ source ansible-crocc/bin/activate

l'activation de l'environnement virtuel se symbolise par les parenthèse en début de prompt *__"(ansible-crocc)"__*. 

![Activation](./images/Ansible/activate.png "Activation")

Installez ensuite Ansible grâce à pip:

        $ python3 -m pip install ansible

Vérifiez l'installation de Ansible grâce à la commande :

        $ ansible --version

### A.2 - Installation de Openstack pour Ansible
Installez maintenant la collection de module OpenStack pour Ansible grâce à Ansible Galaxy:

        $ ansible-galaxy collection install openstack.cloud

Dans la documentation d'Ansible Galaxy pour la collections openstack/cloud, il est stipulé d'installer openstacksdk par pip:

        $ python3 -m pip install openstacksdk

## B - Exécution 
Avant l'exécution de notre premier playbook, nous allons sourcer le fichier d'identification d'application créer dans le *[QuickStart §A.1.2](https://forgemia.inra.fr/crocc-users/support/-/blob/main/Wiki/Quick_Start.md#a12-ajout-dune-clefs-publique-ssh)*. :

        $ source ./monidentifiantapi.sh

Vérifiez le bon fonctionnement avec le playbook verification.yml

        $ ansible-playbook -i localhost, support/Examples/Ansible/playbook/verification.yml

![Vérification](./images/Ansible/verification.png "Vérification")

## C - Création d'instance
Créez votre première instance grâce à Ansible :

        $  ansible-playbook -i localhost, -e "ma_clef_ssh=<le_nom_de_votre_clef_ssh>"  -e "votre_projet=<le_nom_de_votre_projet>" support/Examples/Ansible/playbook/create_instance_all_in_one.yml

![Création de ma première instance par Ansible tout en un](./images/Ansible/createinstanceallinone.png "Création de ma première instance par Ansible tout en un")
