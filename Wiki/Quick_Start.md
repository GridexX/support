# Quick Start 
Voici un tutoriel afin de démarrer rapidement avec OpenStack du CROcc (Cloud Recherche Occitanie).


## A.0 - Access

Vous povuez vous connecter aux deux clouds aux adresse suivantes:

- Cloud Toulouse : [https://cloud.crocc.univ-toulouse.fr/](https://cloud.crocc.univ-toulouse.fr/)
- Cloud Montpellier : [https://crocc-mtp.meso.umontpellier.fr/](https://crocc-mtp.meso.umontpellier.fr/)
## A - Compte 
Tout d'abord nous allons "paramétrer" votre compte, en vous connectant avec la fédération **"EDUGAIN"**. Une fois connecté, vous obtenez la page de vue d'ensemble de votre projet.

![Vue d'ensemble](./images/Compte/02-vue_ensemble.png "Vue d'ensemble")

Sur le bandeau du haut de cette page nous retrouvons les principales informations :
- (1) votre fédération ainsi que le projet sur lequel vous êtes connecté.
- (2) votre nom d'utilisateur

![Bandeau du haut](./images/Compte/02-bandeauduhaut.png "Bandeau du haut")

### A.1 - Création/Ajout d'une clef SSH d'administration
Afin d'administrer vos instances, il faut créer ou ajouter une clef SSH. Pour cela vous allez dans la rubrique "Projet &rarr;  Compute &rarr; **Paires de clés**".
>:warning: *En aucun cas OpenStack ne garde et stocke la clef privée de la paire de clef. Donc en cas de perte de la clef privée, il faudra en refaire une nouvelle.*

![Pairedeclefs](./images/Compte/06-pairedeclefcree.png "Pairedeclefs")

#### A.1.1 - Création d'une paire de clefs SSH
Pour créer une paire de clefs, cliquez sur le bouton "**Créer une paire de clés**" :
- Notez le **nom** de votre paire de clefs
- Choisissez le type de clef en "**Clé SSH**"
- Cliquez sur "**Créer une paire de clés**"
>:exclamation: *A la création d'une paire de clef, il vous sera imposé de télécharger la clef privée de la paire de clef.*

![CreationPairedeclefs](./images/Compte/05-creationclef.png "CreationPairedeclefs")

#### A.1.2 - Ajout d'une clefs publique SSH
Pour importer une clef publique , cliquez sur le bouton "**Importer une clé publique**" :
- Notez le **nom** de votre paire de clefs
- Choisissez le type de clef en "**Clé SSH**"
- Importez votre clef **publique** depuis un fichier ou en copiant/collant le contenu dans l'encadré prévu à cet effet.
- Cliquez sur "**Créer une paire de clés**"
  
![ImportationPairedeclefs](./images/Compte/05-importationclef.png "ImportationPairedeclefs")

### A.2 - Création d'une clef d'identification applicative
Si vous souhaitez vous connecter en CLI au CROcc, vous devez vous créer une clef d'identification applicative. Pour cela allez dans la rubrique "Identité &rarr;  **identifiant d'application**".

![IdentificationApplication](./images/Compte/10-indetnapplicree.png "IdentificationApplication") 

Pour créer un identifiant d'application, veuillez cliquer sur "**Créer un identifiant d'application**"
- Notez le **nom** choisi
- Cliquez sur **Créer un identifiant d'application**
>:warning: *Lors de la création d'identifiant d'application il vous sera proposé de télécharger votre identification (login+password) car en aucun cas OpenStack ne garde et stocke votre mot de passe. Donc en cas de perte de la clef privée, il faudra en refaire une nouvelle.*

![CreationIdentificationApplication](./images/Compte/08-indetnapplicreatation01.png "CreationIdentificationApplication") 

Téléchargez le fichier openrc sur votre machine et cliquez sur "**Fermer**".

![CreationIdentificationApplication](./images/Compte/09-indetnapplicreatation02.png "CreationIdentificationApplication") 

## B - Création d'une première instance
Nous allons maintenant créer notre première instance. Pour cela rendez-vous dans la rubrique "Projet &rarr;  Compute &rarr; **Instances**".

### B.1 - Création d'une instance
Pour créer une instance cliquez sur "**Lancer une instance**".
Remplissez le **Nom de l'instance**, une description et cliquez sur "**Suivant**".

![CreationInstanceDetails](./images/Instance/instance01details.png "CreationInstanceDetails") 

Nous allons maintenant choisir la source (l'os de l'instance) en sélectionnant par exemple l'image **debian-11-20221108-1193** dans la liste des "Disponible". Cliquez aussi sur "**Non**" à la question "Créer un nouveau volume". Puis cliquer sur "**Suivant**".

![CreationInstanceSources](./images/Instance/instance02source.png "CreationInstanceSources") 

Nous allons maintenant choisir le gabarit (le dimensionnement de l'instance) en choisissant par exemple "**m1.small**" dans la liste des gabarits disponibles. Puis cliquer sur "**Suivant**".

![CreationInstanceGabarits](./images/Instance/instance03gabarit.png "CreationInstanceGabarits")

Pour la partie Réseau par défaut OpenStack sélectionne automatiquement si vous n'avez qu'un seul réseau. Sinon sélectionnez le réseau voulu. Puis cliquer sur "**Suivant**".

![CreationInstanceReseau](./images/Instance/instance04reseau.png "CreationInstanceReseau")

Nous allons passer les sections "Ports réseaux" et "Groupes de sécurité", pour aller à la section "Key Pair" et vérifier que votre clef ssh soit bien allouée à votre instance. "Puis cliquer sur "**Lancer Instance**".

![CreationInstanceClefSSH](./images/Instance/instance07clefssh.png "CreationInstanceClefSSH")

Voila vous venez de créer une première instance. Vous pouvez la retrouver sur la page de gestion des instances.

![InstanceCreer](./images/Instance/instancelancee.png "InstanceCreer")

### B.2 - Création et Attachement d'un volume Persistent
Le volume (disque) système de votre instance est de type éphémère. Pour OpenStack éphémère signifie que si vous supprimez votre instance, le volume est aussi supprimé. 

>:warning: Un volume persistent est comme une clef usb, elle ne peut être attachée qu'à une seule instance à la fois.

#### B.2.1 - Création d'un volume Persistent
Pour créer un volume persistent (qui résiste à la suppression de votre instance) allez dans la rubrique 
"Projet &rarr; Volumes &rarr; **Volumes**" et cliquez sur "**Créer un volume**". Dans la page de création de volume, remplissez le **nom du volume**, sa description, sa **taille** et cliquez sur "**Créer le volume**".

![VolumePersistentCreation](./images/Instance/volumecreation.png "VolumePersistentCreation")

#### B.2.2 - Attachement d'un volume Persistent
Pour attacher un volume à une instance, sélectionner dans la liste de la colonne "Actions" du volume concerné "**Gérer les attachements**"

![VolumePersistentAttachement](./images/Instance/volumeattachement.png "VolumePersistentAttachement")

Dans le pop-up de gestion des attachements du volume, sélectionnez l'instance à laquelle vous souhaitez attacher le volume persistent. Puis cliquez sur "Attacher le volume".

![VolumePersistentAttachementInstance](./images/Instance/volumeattachementinstance.png "VolumePersistentAttachementInstance")

### B.3 - Création et Association d'une adresse IP flottante
Une fois créée votre instance peut accéder à internet mais ne peut-être accedée depuis internet. Si vous souhaitez accéder à votre instance directement depuis internet (e.g : serveur web, serveur ssh, etc...) il vous faut lui assigner une adresse ip flottante.

>:warning: *lors de la suppression d'une instance à laquelle est associée une adresse ip flottante, l'adresse reste réservée au projet. Si vous n'en avez plus besoin, il faut libérer l'adresse ip grâce au menu "Action" en face de l'adresse ip concernée.*

#### B.3.1 - Demande d'adresse IP flottante
Nous allons tout d'abord demander une adresse ip publique au CROcc. Pour cela allez dans la rubrique "Projet &rarr; Réseaux &rarr; **IP flottantes**". Puis cliquez sur "**Allouer une adresse IP au projet**". Sélectionner ensuite le "Pool" **public1** et renseigner une description. Puis cliquer sur "**Allocation d'IP**".

![IPFlottanteCreation](./images/Instance/creationipflottante.png "IPFlottanteCreation")

#### B.3.2 - Association d'une adresse IP flottante
Maintenant que vous avez une adresse ip publique pour votre projet nous allons l'associer à votre instance. Pour cela nous allons cliquer sur le bouton "**Associer**" en face de l'adresse IP.

![IPFlottante](./images/Instance/ipflottante.png "IPFlottante")

Sélectionnez l'instance concernée dans la liste "Port à associer", puis cliquez sur "**Associer**".

![IPFlottanteAssociation](./images/Instance/ipflottanteassocier.png "IPFlottanteAssociation")

### B.4 - Ouverture de flux réseau pour votre instance
Maintenant que votre instance est accessible depuis internet (instance et adresse IP flottante), nous allons ouvrir les flux du service proposé par votre instance (ici le service ssh sur le port 22). 

#### B.4.1 - Création du groupe de sécurité
Pour créer un groupe de sécurité, allez dans la rubrique "Projet &rarr; Réseau &rarr; **Groupes de sécurité**", cliquez sur "**Créer un groupe de sécurité**". Remplissez le **nom** du groupe de sécurité et cliquez sur "**Créer un groupe de sécurité**" 

![GroupeSecuriteCReation](./images/Instance/groupesecuritecreation.png "GroupeSecuriteCReation")

Ajoutez ensuite une règle à votre votre groupe de sécurité en cliquant sur "**Ajouter une règle**". Dans la page d'ajout de règle, sélectionnez un choix (e.g : **SSH**), puis cliquez sur "**Ajouter**"

![GroupeSecuriteAjoutRegle](./images/Instance/groupesecuriteajoutregle.png "GroupeSecuriteAjoutRegle")

#### B.4.2 - Ajouter le groupe de sécurité à votre instance
Pour ajouter ce groupe de sécurité à votre instance, allez dans la rubrique "Projet &rarr; Compute &rarr; **Instances**". En face de votre instance, dans la colonne "Action"et sélectionnez **Éditer les groupes de sécurité**.

![GroupeSecurite](./images/Instance/groupesecurite.png "GroupeSecurite")

Basculer le groupe de sécurité précédemment créé vers la droite puis cliquez sur "**Enregistrer**".

![GroupeSecuriteAssocierInstance](./images/Instance/ajoutgroupesecurite.png "GroupeSecuriteAssocierInstance")

#### B.4.3 - Vérification d'ouverture de flux vers mon instance
Avec votre outil ssh préféré, connectez vous à votre instance par l'ip flottante précédemment associée. Pour l'authentification, utiliser root et votre clef SSH privée dans un premier temps. 
> $ ssh -i /cheminversmaclefssh/maclefsshprivee root@monipflottante

![Verifroot](./images/Instance/verifroot.png "Verifroot")

En vous connectant en tant que l’utilisateur root, l'instance vous demande d'utiliser plutôt l'utilisateur "debian". Connectez vous donc avec l'utilisateur demandé, vous êtes connecté. Grâce à la commande **sudo -l**, vous pouvez voir vos droits sudo sur la machine (tous sans mot de passe) et grâce à *lsblk* par exemple vous pouvez retrouver le volume persistent connecté en /dev/sdb.

![Verif](./images/Instance/verif.png "Verif")


