# k8s-doc

## Déployer une application dans un pod

Les pods sont la plus petite unité de déploiement dans Kubernetes, et ils représentent un ou plusieurs conteneurs Docker fonctionnant ensemble sur un seul noeud de la grappe Kubernetes. Les pods sont souvent utilisés pour créer des microservices, qui sont des applications divisées en plusieurs petits services autonomes qui travaillent ensemble pour répondre à une demande.

Un exemple de pod dans Kubernetes pourrait être un pod contenant un conteneur qui exécute un serveur web et un autre conteneur qui exécute un service de base de données. Les deux conteneurs fonctionnent ensemble dans le même pod pour fournir une application web complète.

Voici un exemple de fichier YAML qui décrit un pod contenant deux conteneurs, un conteneur pour un serveur web Nginx et un autre conteneur pour un service Redis:

```shell
apiVersion: v1
kind: Pod
metadata:
  name: webapp
  labels:
    app: myapp
spec:
  containers:
  - name: nginx
    image: nginx:latest
    ports:
    - containerPort: 80
  - name: redis
    image: redis:latest
```

Ce fichier YAML décrit un pod nommé webapp, qui contient deux conteneurs nommés nginx et redis. Le conteneur nginx utilise l'image Docker nginx:latest et expose le port 80, tandis que le conteneur redis utilise l'image Docker redis:latest.

Pour créer ce pod à partir du fichier YAML, vous pouvez exécuter la commande suivante dans votre terminal Kubernetes:

```shell
kubectl apply -f webapp.yaml
```

Cela créera le pod webapp avec les deux conteneurs nginx et redis en cours d'exécution. Vous pouvez ensuite gérer ce pod à l'aide de commandes Kubernetes telles que kubectl get pods pour afficher les pods en cours d'exécution, kubectl describe pod webapp pour afficher des informations détaillées sur le pod webapp, et kubectl delete pod webapp pour supprimer le pod webapp.

## Service

Les services sont une autre ressource importante dans Kubernetes qui permettent d'exposer des pods et de permettre la communication entre eux. Les services sont des objets abstraits qui fournissent une adresse IP fixe et un nom pour accéder aux pods, et qui gèrent le trafic entrant vers ces pods.

Voici un exemple de fichier YAML qui décrit un service dans Kubernetes:

```shell
apiVersion: v1
kind: Service
metadata:
  name: webapp-service
spec:
  selector:
    app: myapp
  ports:
  - name: http
    port: 80
    targetPort: 80
  type: ClusterIP
```

Ce fichier YAML décrit un service nommé webapp-service qui expose le port 80 et dirige le trafic vers les pods qui ont l'étiquette app: myapp. Le service utilise le type ClusterIP, qui signifie qu'il crée une adresse IP interne au cluster Kubernetes pour accéder aux pods.

Pour créer ce service à partir du fichier YAML, vous pouvez exécuter la commande suivante dans votre terminal Kubernetes:

```shell
kubectl apply -f webapp-service.yaml
```

Cela créera le service webapp-service dans Kubernetes. Les pods qui ont l'étiquette app: myapp seront automatiquement inclus dans le service et pourront être accessibles à partir de l'adresse IP et du nom du service webapp-service. Vous pouvez vérifier que le service fonctionne correctement en exécutant la commande kubectl describe service webapp-service, qui affichera des informations sur le service, y compris son adresse IP et les ports exposés.

### Service: LoadBalancer
Le service de type LoadBalancer de Kubernetes permet d'exposer des services de manière externe à un cluster Kubernetes. Lorsqu'un service est exposé en tant que LoadBalancer, Kubernetes crée un équilibreur de charge (load balancer) externe pour acheminer le trafic vers les instances du service.

```shell
apiVersion: v1
kind: Service
metadata:
  name: mon-service-web
spec:
  type: LoadBalancer
  ports:
    - name: http
      port: 80
      targetPort: 8080
  selector:
    app: mon-application-web
```
Dans cet exemple, le service de type LoadBalancer est nommé "mon-service-web" et écoute le port 80 pour le trafic HTTP. Le port 80 est redirigé vers le port 8080 de l'application qui est identifiée par l'étiquette "app: mon-application-web".

Une fois que vous avez créé la définition de service, vous pouvez déployer votre application avec Kubernetes en lui appliquant la même étiquette. Kubernetes va alors créer un équilibreur de charge externe et le configurer pour diriger le trafic entrant vers les instances de l'application.

Après quelques instants, vous pouvez afficher les détails du service de type LoadBalancer à l'aide de la commande suivante :

```shell 
kubectl get services mon-service-web
```

### Service: NodePort

Le type NodePort est un type de service Kubernetes qui expose une application en créant un port sur chaque nœud du cluster, permettant ainsi d'accéder à l'application depuis l'extérieur du cluster.
Voici un exemple de définition de service NodePort dans un fichier YAML :

```shell
apiVersion: v1
kind: Service
metadata:
  name: mon-service
spec:
  type: NodePort
  ports:
    - name: http
      port: 80
      targetPort: 8080
  selector:
    app: mon-application
```
Dans cet exemple, le service s'appelle "mon-service". Le champ "type" est défini sur "NodePort" pour indiquer que c'est un service NodePort. Le port "80" est exposé sur chaque nœud du cluster, et le trafic est acheminé vers le port "8080" de l'application, qui est identifié par l'étiquette "app: mon-application".

Une fois que le service est créé, vous pouvez obtenir l'adresse IP de n'importe quel nœud du cluster et utiliser le port NodePort pour accéder à l'application. Par exemple, si l'adresse IP d'un nœud est "10.0.0.1", et que le port NodePort est "30000", vous pouvez accéder à l'application en utilisant l'URL "http://10.0.0.1:30000".


### La haute disponibilité avec le type "Deployment"
Un "Deployment" est un ensemble de réplicas identiques d'une application qui s'exécutent dans un cluster Kubernetes. Il permet de spécifier les caractéristiques de l'application, telles que le nombre de réplicas souhaité, les images Docker utilisées, les ports à exposer, etc.

Lorsqu'un "Deployment" est créé, Kubernetes crée un ensemble de Pods (des groupes de conteneurs) qui contiennent les réplicas de l'application. Si l'un de ces Pods échoue, Kubernetes le remplace automatiquement par un nouveau Pod, garantissant ainsi la haute disponibilité de l'application.

De plus, les "Deployments" permettent de mettre à jour facilement une application en utilisant des stratégies de rolling-update ou de blue-green deployment, qui permettent de mettre à jour progressivement les réplicas de l'application sans interruption de service.

En somme, les "Deployments" de Kubernetes permettent de simplifier la gestion des applications et de garantir leur haute disponibilité.

```shell
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
```
Dans cet exemple, nous avons défini les caractéristiques du "Deployment" :

- Le nom du "Deployment" est "nginx-deployment".
- Le nombre de réplicas souhaité est de 2.
- Nous avons spécifié le sélecteur "matchLabels" qui identifie les Pods que le "Deployment" doit gérer. Dans ce cas, nous avons choisi "app: nginx".
- Nous avons créé une spécification de "template" qui décrit la configuration de chaque Pod créé. Dans ce cas, nous avons spécifié un conteneur Nginx avec la dernière image Docker disponible.
- Nous avons exposé le port 80 du conteneur pour que les clients puissent accéder à l'application.

### Namespaces:

Les namespaces sont l'unité d'isolement et de collaboration dans kubernetes:

- a un ou plusieurs Users.
- a des quota de ressources alloué aux applications du namespace.
- a des règle de sécurité sur les ressources dans le namespace. 

```shell
kubectl create namespace test
```


