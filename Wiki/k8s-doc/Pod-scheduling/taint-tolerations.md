Dans Kubernetes, les taints et les tolerations sont utilisés pour restreindre les nœuds sur lesquels un pod peut être planifié.

Un taint est une étiquette appliquée à un nœud qui indique qu'il ne peut pas accepter certains pods, à moins que ces derniers ne disposent de la tolérance appropriée. Par exemple, vous pouvez appliquer un taint à un nœud qui spécifie qu'il ne peut pas exécuter de pods avec une certaine quantité de ressources, ou avec un certain type d'environnement.

D'autre part, une tolérance est une étiquette appliquée à un pod qui lui permet d'être planifié sur un nœud marqué d'un taint spécifique. Une tolérance peut être définie pour permettre à un pod d'être planifié sur un nœud avec un taint spécifique en définissant la tolérance appropriée.

Voici un exemple : supposons que vous avez un cluster Kubernetes avec deux nœuds, "node-1" et "node-2". Si vous souhaitez que certains pods ne soient exécutés que sur "node-1", vous pouvez appliquer un taint à "node-2" pour indiquer qu'il ne peut pas accepter ces pods. Vous pouvez appliquer le taint de la manière suivante:

```shell
kubectl taint nodes node-2 key=value:NoSchedule
```
Cela signifie que les pods sans tolérance appropriée ne seront pas planifiés sur "node-2".

Ensuite, pour autoriser les pods à être planifiés sur "node-2", vous pouvez définir une tolérance appropriée sur les pods en utilisant l'annotation "tolerations" dans la configuration du pod. Par exemple:

```shell
apiVersion: v1
kind: Pod
metadata:
  name: example-pod
spec:
  containers:
  - name: nginx
    image: nginx
  tolerations:
  - key: "key"
    value: "value"
    effect: "NoSchedule"
```

Cela signifie que le pod "example-pod" peut être planifié sur "node-2" malgré le taint, car il dispose d'une tolérance avec la clé "key", la valeur "value" et l'effet "NoSchedule".