Les ConfigMaps dans Kubernetes sont des objets qui stockent des données de configuration sous forme de paires clé-valeur. Les ConfigMaps peuvent être utilisées pour stocker des variables d'environnement, des fichiers de configuration ou tout autre type de données de configuration.

Voici un exemple de ConfigMap dans Kubernetes :

Supposons que vous ayez un fichier de configuration config.yaml pour votre application. Vous pouvez créer un ConfigMap en exécutant la commande suivante :

```shell
kubectl create configmap my-config --from-file=config.yaml
```

Cette commande va créer un ConfigMap appelé my-config et y stocker le contenu du fichier config.yaml sous forme de clé-valeur.

Vous pouvez ensuite utiliser ce ConfigMap dans un Pod en spécifiant la clé correspondante dans la section env ou volumeMounts du manifeste Pod.

Exemple avec la section env :

```shell
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    env:
    - name: CONFIG_VAR
      valueFrom:
        configMapKeyRef:
          name: my-config
          key: config.yaml
```

Dans cet exemple, nous avons créé un Pod avec un conteneur et défini une variable d'environnement CONFIG_VAR qui récupère sa valeur à partir de la clé config.yaml de notre ConfigMap my-config.

Exemple avec la section volumeMounts :
```shell
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    volumeMounts:
    - name: config-volume
      mountPath: /etc/config
  volumes:
  - name: config-volume
    configMap:
      name: my-config
```

Dans cet exemple, nous avons créé un Pod avec un conteneur et un volume monté. Nous avons créé un volume à partir de notre ConfigMap my-config et monté ce volume dans notre conteneur à /etc/config. Les données de configuration stockées dans notre ConfigMap seront donc accessibles à partir du chemin /etc/config dans notre conteneur.

