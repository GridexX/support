Les Secrets dans Kubernetes sont des objets qui stockent des données sensibles, telles que des mots de passe, des clés SSH ou des certificats TLS. Les Secrets sont utilisés pour stocker des données de manière sécurisée, de sorte qu'elles ne soient pas facilement accessibles ou exposées.

Voici un exemple de Secret dans Kubernetes :

Supposons que vous ayez un mot de passe pour votre base de données que vous souhaitez stocker de manière sécurisée. Vous pouvez créer un Secret en exécutant la commande suivante :

```shell
kubectl create secret generic my-db-secret --from-literal=password=MySecurePassword
```

Cette commande va créer un Secret appelé my-db-secret et y stocker la valeur MySecurePassword sous la clé password.

Vous pouvez ensuite utiliser ce Secret dans un Pod en spécifiant la clé correspondante dans la section env ou volumeMounts du manifeste Pod.

Exemple avec la section env :

```shell
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    env:
    - name: DB_PASSWORD
      valueFrom:
        secretKeyRef:
          name: my-db-secret
          key: password
```

Dans cet exemple, nous avons créé un Pod avec un conteneur et défini une variable d'environnement DB_PASSWORD qui récupère sa valeur à partir de la clé password de notre Secret my-db-secret.

Exemple avec la section volumeMounts :

```shell
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    volumeMounts:
    - name: secret-volume
      mountPath: /etc/secret
  volumes:
  - name: secret-volume
    secret:
      secretName: my-db-secret
```
Dans cet exemple, nous avons créé un Pod avec un conteneur et un volume monté. Nous avons créé un volume à partir de notre Secret my-db-secret et monté ce volume dans notre conteneur à /etc/secret. Les données sensibles stockées dans notre Secret seront donc accessibles à partir du chemin /etc/secret dans notre conteneur.
