## Ingress vs Routes

- Dans k8s : possibilité de déployer un IngressController ( un Load Balancer layer 7 http/https ), la configuration se fait avec les objets Ingress.
- Openshift intègre un IngressController HAProxy appeler router, la  configuration se fait avec les objets Route.

![image-2.png](./image-2.png)
