Dans Kubernetes, un ingress est un objet qui permet de gérer les règles de routage du trafic entrant vers les services d'un cluster Kubernetes. Il est souvent utilisé pour exposer des services HTTP ou HTTPS à l'extérieur du cluster.

Prenons un exemple concret pour mieux comprendre. Supposons que vous avez un cluster Kubernetes qui héberge plusieurs services, tels qu'un service de messagerie, un service de commande et un service de facturation. Vous souhaitez maintenant exposer ces services à l'extérieur du cluster, afin que les clients puissent y accéder.

Pour cela, vous pouvez utiliser un objet Ingress dans Kubernetes. Voici à quoi cela pourrait ressembler :

```shell
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: mon-ingress
spec:
  rules:
  - host: monsite.com
    http:
      paths:
      - path: /messagerie
        pathType: Prefix
        backend:
          service:
            name: service-messagerie
            port:
              name: http
      - path: /commande
        pathType: Prefix
        backend:
          service:
            name: service-commande
            port:
              name: http
      - path: /facturation
        pathType: Prefix
        backend:
          service:
            name: service-facturation
            port:
              name: http
```
Dans cet exemple, nous avons créé un objet Ingress nommé "mon-ingress" qui utilise des règles pour spécifier comment le trafic doit être routé vers les services de messagerie, de commande et de facturation.

Plus précisément, nous avons configuré l'Ingress pour écouter les requêtes HTTP qui arrivent à l'hôte "monsite.com". Ensuite, nous avons défini trois chemins différents ("path") pour chaque service et spécifié le service et le port correspondant pour chacun d'eux.

Ainsi, lorsque les clients accèdent à "monsite.com/messagerie", "monsite.com/commande" ou "monsite.com/facturation", le trafic est routé vers les services correspondants dans le cluster Kubernetes.

En résumé, l'objet Ingress de Kubernetes permet de configurer facilement la gestion du trafic entrant vers les services d'un cluster Kubernetes, en utilisant des règles de routage basées sur les noms de domaine et les chemins d'accès.
