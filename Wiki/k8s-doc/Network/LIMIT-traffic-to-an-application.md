![IMAGE_DESCRIPTION](net2.png)

Restreindre les connexions à une base de données uniquement à l'application qui l'utilise.

```shell
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: api-allow
spec:
  podSelector:
    matchLabels:
      app: bookstore
      role: api
  ingress:
  - from:
      - podSelector:
          matchLabels:
            app: bookstore
```
