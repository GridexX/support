NetworkPolicy est une ressource Kubernetes qui permet de spécifier les règles de sécurité pour le trafic réseau à l'intérieur du cluster. Avec NetworkPolicy, vous pouvez spécifier quelles ressources peuvent communiquer entre elles, quelles ports peuvent être utilisés et quelles adresses IP sont autorisées à accéder à une application.

```shell
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-nginx
spec:
  podSelector:
    matchLabels:
      app: nginx
  policyTypes:
  - Ingress
  ingress:
  - from:
    - ipBlock:
        cidr: 192.168.0.0/24
    ports:
    - port: 80
      protocol: TCP
```

Ce fichier YAML crée une NetworkPolicy qui permet le trafic entrant sur le port 80 uniquement pour les adresses IP dans la plage 192.168.0.0/24 pour les pods qui ont le label "app: nginx".

En résumé, NetworkPolicy vous permet de spécifier des règles de sécurité pour le trafic réseau entre les ressources dans votre cluster Kubernetes, vous permettant ainsi de sécuriser votre environnement Kubernetes en contrôlant les accès
