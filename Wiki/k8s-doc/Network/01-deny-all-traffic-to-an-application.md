
![IMAGE_DESCRIPTION](net1.png)

Cas 1: 
Vous voulez faire fonctionner un pod et empêcher les autres pods de communiquer avec lui.

```shell
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: web-deny-all
spec:
  podSelector:
    matchLabels:
      app: web
  ingress: []
```
