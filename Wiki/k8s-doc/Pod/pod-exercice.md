1.	Créer un namespace test
2.	Se placer dans le namespace test, pour éviter d’avoir à préciser -n test à chaque commande
3.	Créer un pod dans le namespace test avec l’image openshift/origin-docker-registry:v0.6.2 le pod doit être à l’état Running
4.	Quelle est l’ip du pod ?
5.	Obtenir le pod au format yaml et sauvegarder la ressource dans le fichier pod.yaml
6.	Changer l’image openshift/origin-docker-registry:v0.6.2 par openshift/origin-docker-registry que ce passe - t – il ?

SOLUTION: 
1.	oc create ns test
2.	oc project test
3.	oc run test  --image openshift/origin-docker-registry:v0.6.2
4.	oc describe pod test  | grep IP
5.	oc get pod test -o yaml >pod.yaml
6.	vi pod.yaml, changer openshift/origin-docker-registry:v0.6.2 par openshift/origin-docker-registry (alternative oc edit pod test)
oc apply –f pod.yaml

La spec du pod est immutable, pour changer de conteneur, il faut kill le pod et en recréer un autre.
