
Kubernetes est un système de gestion de conteneurs qui permet de déployer et de gérer des applications dans un environnement de production. Pour optimiser les performances et la stabilité de l'application, Kubernetes propose la notion de "request" et de "limit".

Les "requests" et les "limits" sont des paramètres configurables pour les ressources consommées par un conteneur dans un pod Kubernetes. Les "requests" représentent la quantité minimale de ressources dont le conteneur a besoin pour fonctionner, tandis que les "limits" représentent la quantité maximale de ressources que le conteneur peut utiliser.

Par exemple, si vous avez une application qui nécessite un minimum de 1 Go de RAM pour fonctionner, vous pouvez définir une "request" de 1 Go pour la mémoire. Si la demande dépasse cette limite, Kubernetes allouera davantage de ressources pour répondre à cette demande. De même, si vous souhaitez limiter la quantité de ressources qu'un conteneur peut utiliser, vous pouvez définir une "limit" pour la quantité maximale de RAM.

Voici un exemple de spécification de pod Kubernetes qui utilise des "requests" et des "limits" pour la mémoire et le CPU :

```shell
apiVersion: v1
kind: Pod
metadata:
  name: my-app
spec:
  containers:
  - name: my-app-container
    image: my-app-image
    resources:
      requests:
        memory: "1Gi"
        cpu: "500m"
      limits:
        memory: "2Gi"
        cpu: "1"
```
Dans cet exemple, la "request" de mémoire est de 1 Go et la "limit" de mémoire est de 2 Go. La "request" de CPU est de 500 milli-CPU (ou 0,5 CPU) et la "limit" de CPU est de 1 CPU. Ces paramètres aident Kubernetes à allouer les ressources nécessaires pour faire fonctionner l'application de manière optimale tout en évitant de dépasser les limites de ressources définies.