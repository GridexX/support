Le but de ce TP est de manipuler les object PersistantVolume (PV) et PersistantVolumeClaim (PVC)

1.	Créer un PV appelé hostpath-volume 
-	de capacité 5Gi
-	en mode ReadWriteOnce 
-	utilisant la storageClassName manual
-	et avec une configuration hostPath suivante :
```shell
hostPath:
    path: "/mnt/data/hostpath-exemple"
    type: DirectoryOrCreate
```
2.	Créer un PVC appelé hostpath-volume-claim
-	dans votre namespace attitré
-	utilisant la storageClassName manual
-	requêttant 10Mi de stockage
-	en mode ReadWriteOnce

3.	Vérifier la bonne liaison du PVC avec le PV
