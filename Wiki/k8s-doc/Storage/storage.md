```shell
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-pv
spec:
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: my-storage-class
  hostPath:
    path: /mnt/data
```
---
```shell
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
  storageClassName: my-storage-class
```
Dans cet exemple, nous créons un objet PersistentVolume avec le nom "my-pv", une capacité de stockage de 10 gigaoctets (Gi) et un accès en lecture/écriture unique (ReadWriteOnce). La stratégie de récupération de volume persistant (persistentVolumeReclaimPolicy) est définie sur "Retain", ce qui signifie que le volume ne sera pas supprimé automatiquement lorsqu'il n'est plus utilisé. Nous avons également défini une classe de stockage nommée "my-storage-class" et utilisé un hôtePath pour définir l'emplacement du stockage.

Ensuite, nous avons créé un objet PersistentVolumeClaim avec le nom "my-pvc". Le PVC demande un espace de stockage de 5 Gi et utilise également la classe de stockage "my-storage-class" pour associer ce PVC au PV correspondant. Les accessModes sont également définis sur ReadWriteOnce, ce qui signifie que le PVC peut être monté en lecture/écriture sur un seul noeud à la fois.

En utilisant cette configuration, Kubernetes provisionnera automatiquement un volume persistant qui répond aux besoins du PVC et qui est associé à la classe de stockage spécifiée. Le PVC pourra ensuite être monté sur un pod et utilisé pour stocker des données.

Et pour savoir le nom de storageClass à utiliser:

```shell
oc get sc
```
