1 & 2 :

```shell
kind: PersistentVolume
metadata:
  name: hostpath-volume 
  labels:
    type: local
spec:
  storageClassName: manual 
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce 
  hostPath:
    path: "/mnt/data/hostpath-exemple"
    type: DirectoryOrCreate
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: hostpath-volume-claim
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: manual
```
3-
```sehll
 [tducrot-adlere.fr@bastion ~]$ oc get pv
NAME              CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                           STORAGECLASS   REASON   AGE
hostpath-volume   1Gi        RWO            Retain           Bound    default/hostpath-volume-claim   manual                  2m59s
```

```shell
[tducrot-adlere.fr@bastion ~]$ oc get pvc
NAME                    STATUS   VOLUME            CAPACITY   ACCESS MODES   STORAGECLASS   AGE
hostpath-volume-claim   Bound    hostpath-volume   1Gi        RWO            manual         113s	
```
