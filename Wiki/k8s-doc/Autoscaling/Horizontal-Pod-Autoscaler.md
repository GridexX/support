La HPA (Horizontal Pod Autoscaler) de Kubernetes est un mécanisme qui permet à votre cluster Kubernetes de scaler automatiquement le nombre de pods d'une application en fonction de la demande en ressources.

Concrètement, la HPA surveille en permanence la consommation de ressources de votre application, telle que la CPU ou la mémoire, et ajuste automatiquement le nombre de pods en fonction des seuils définis par l'utilisateur. Si la charge de votre application augmente, la HPA ajoutera des pods supplémentaires pour répondre à la demande, tandis que si la charge diminue, elle réduira automatiquement le nombre de pods.

Voici un exemple simple pour illustrer le fonctionnement de la HPA de Kubernetes :

Supposons que vous avez une application web déployée sur Kubernetes, qui consiste en plusieurs pods. Vous pouvez définir un seuil de consommation de CPU à 80% pour votre HPA. Cela signifie que lorsque la consommation de CPU de votre application atteint 80%, la HPA ajoutera automatiquement des pods supplémentaires pour répondre à la demande.

Imaginons maintenant que votre application subit une forte demande pendant un certain temps, et que la consommation de CPU atteint les 80%. La HPA détecte cela et ajoute automatiquement des pods supplémentaires pour gérer la charge supplémentaire. Cela permet à votre application de continuer à fonctionner sans interruption et d'éviter les temps d'arrêt ou les ralentissements pour les utilisateurs.

Plus tard, lorsque la demande diminue et que la consommation de CPU tombe en dessous de 80%, la HPA réduira automatiquement le nombre de pods pour économiser des ressources et éviter les coûts inutiles.

En somme, la HPA de Kubernetes permet de rendre l'infrastructure de votre application plus agile et plus flexible, en ajustant automatiquement le nombre de pods pour répondre à la demande en temps réel.


```shell
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: nginx
spec:
  maxReplicas: 10
  minReplicas: 2
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx
  targetCPUUtilizationPercentage: 80
```
