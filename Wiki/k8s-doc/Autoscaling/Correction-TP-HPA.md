1. oc create deployment nginx --image twalter/openshift-nginx
2. 
```shell
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: nginx
spec:
  maxReplicas: 10
  minReplicas: 2
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx
  targetCPUUtilizationPercentage: 80
```

3. oc get hpa

NAME    REFERENCE          TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
nginx   Deployment/nginx   <unknown>/80%   2         10        	2          96s


## HPA dans OKD avec l'UI

![image info](hpa1.png)
![image info](hpa2.png)
![image info](hpa3.png)

