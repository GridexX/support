1.	Créer un déployment nginx 
-	Appelé nginx
-	Dans votre namespace
-	utilisant l’image twalter/openshift-nginx
-	avec 1 réplica

2.	Créer un HorizontalPodAutoscaler
-	Max 5 réplica
-	Min 2 réplica
-	targetCPUUtilizationPercentage 80
3.	Attendre que le HPA fasse effet, vous devez obtenir 2 réplicas de Pod nginx
