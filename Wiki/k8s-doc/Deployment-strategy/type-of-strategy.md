Rolling Update :
La stratégie de déploiement Rolling Update est la stratégie par défaut de Kubernetes. Elle déploie une nouvelle version de l'application en remplaçant progressivement les anciens conteneurs par les nouveaux. Cette stratégie permet de garantir une disponibilité continue de l'application pendant la mise à jour. Vous pouvez configurer le nombre de pods à mettre à jour simultanément ainsi que la vitesse de déploiement pour contrôler le processus de mise à jour.

Recreate :
La stratégie de déploiement Recreate consiste à supprimer tous les pods de la version précédente de l'application avant de déployer la nouvelle version. Cette stratégie est simple et rapide, mais elle entraîne une interruption temporaire de l'application pendant le déploiement.

Canary :
La stratégie de déploiement Canary consiste à déployer une nouvelle version de l'application pour un petit groupe d'utilisateurs avant de la déployer pour l'ensemble de l'utilisateur. Cette stratégie permet de tester la nouvelle version de l'application en production avec un nombre limité d'utilisateurs avant de la déployer pour tout le monde.

Blue/Green :
La stratégie de déploiement Blue/Green consiste à déployer une nouvelle version de l'application dans un environnement de production parallèle (appelé "Green") et à basculer progressivement le trafic utilisateur vers cet environnement. Si quelque chose se passe mal avec la nouvelle version de l'application, vous pouvez facilement basculer le trafic vers l'environnement de production précédent (appelé "Blue") sans interruption de service.
