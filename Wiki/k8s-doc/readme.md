
# Documentation kubernetes

## OVERVIEW

Si vous avez une application et que vous souhaitez la placer dans un cluster kubernetes, vous pouvez commencer par l'encapsuler dans un POD.

Avant de déployer un POD, assurez-vous d'avoir une image docker stockée dans un registre global/privé qui contient votre application et qui est accessible depuis le cluster.

Pour plus d'info sur les pod regardez le folder POD

Après la création des pods, il est nécessaire de créer un service, qui vous donnera accès à votre application.

Pour plus d'informations sur les services et leurs types, consultez l'aperçu du dossier qui contient un exemple de pod et de son service (vous devez choisir le bon type de service en fonction de vos besoins et du type d'application), l'avant-dernière section de l'aperçu du dossier, nous avons encapsulé le POD dans un DEPLOYMENT pour assurer une haute disponibilité.

Grâce au type DEPLOYMENT, nous avons créé des répliques de notre application, sauf que l'application n'est pas auto-échelonnable, si vous voulez que votre application prenne en charge l'échelonnement vers le haut ou vers le bas, consultez le dossier Autoscaling.

Supposons que vous ayez loué un cluster avec 3 travailleurs, mais que ces 3 travailleurs n'aient pas les mêmes caractéristiques techniques et que vous souhaitiez placer une application (POD) sur un travailleur spécifique, regardez le dossier Pod-scheduling.

Si vous avez besoin de la persistance des volumes et de la gestion des instantanés de vos volumes, regardez du côté des dossiers Storage / VolumeSnapshot.

Si vous avez besoin d'injecter des mots de passe ou des fichiers de configuration au niveau du pod, consultez le dossier configurationfile.

Afin de limiter la consommation d'execisf des pods, regardez dans le dossier requests-limits.

Si vous avez des applications de base de données / statefulset, regardez dans le dossier statefulset.


Pour la gestion des utilisateurs de votre cluster et des droits attribués à chaque utilisateur, voir le dossier de gestion des  user-management.


Pour l'exposition de votre application sur internet et vous voulez les mettre derrière un proxy regardez le folder proxy.
