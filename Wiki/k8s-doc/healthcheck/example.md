Supposons que vous ayez une application web dans un conteneur Docker qui expose un service HTTP sur le port 8080. Vous voulez vous assurer que le conteneur est en mesure de répondre aux requêtes HTTP et qu'il peut également se connecter à une base de données PostgreSQL avant d'accepter du trafic.

Pour configurer une "liveness probe", vous pouvez ajouter les lignes suivantes à votre fichier de configuration Kubernetes (par exemple, deployment.yaml) :

```shell
livenessProbe:
  httpGet:
    path: /healthz
    port: 8080
  initialDelaySeconds: 15
  periodSeconds: 30
```

Ceci configurerait une sonde HTTP GET pour envoyer une requête à l'URL "/healthz" toutes les 30 secondes et attendre une réponse avec un code HTTP 200 pour indiquer que le conteneur est en vie. Si le conteneur ne répond pas après 15 secondes lors du premier test, Kubernetes considérera que le conteneur a échoué et tentera de le redémarrer.

Pour configurer une "readiness probe", vous pouvez ajouter les lignes suivantes à votre fichier de configuration Kubernetes :

```shell
readinessProbe:
  exec:
    command:
    - sh
    - -c
    - psql -h postgres -U postgres -c 'SELECT 1'
  initialDelaySeconds: 30
  periodSeconds: 10
```

Ceci configurerait une sonde exécutable pour vérifier si le conteneur peut se connecter à une base de données PostgreSQL en exécutant une commande psql pour effectuer une requête SELECT sur la base de données. Si la commande renvoie un code de sortie 0, le conteneur sera considéré comme prêt à accepter du trafic. Si la sonde échoue pendant la période de démarrage initial de 30 secondes, Kubernetes considérera que le conteneur n'est pas prêt et n'enverra pas de trafic vers ce conteneur jusqu'à ce qu'il soit prêt.

En utilisant ces sondes, vous pouvez être sûr que votre application web fonctionne correctement et est prête à accepter du trafic avant de recevoir des demandes de l'utilisateur, ce qui garantit une expérience utilisateur sans temps d'arrêt indésirable.
