Un StatefulSet est un objet Kubernetes qui permet de gérer des applications étatiques (avec des données persistantes). Il garantit que chaque instance de l'application a un nom unique et un état stable, même en cas de redémarrage ou de mise à jour. Contrairement à une configuration classique de déploiement, les pods d'un StatefulSet sont créés et mis à jour dans un ordre séquentiel.

Un exemple d'utilisation d'un StatefulSet serait une base de données distribuée comme Cassandra ou MongoDB. Chaque instance de la base de données doit avoir un nom unique et des données persistantes, et il est important de garantir que chaque instance est disponible et stable.

Prenons l'exemple de Cassandra :

Créer un fichier YAML pour définir le StatefulSet :
```shell
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: cassandra
spec:
  serviceName: cassandra
  replicas: 3
  selector:
    matchLabels:
      app: cassandra
  template:
    metadata:
      labels:
        app: cassandra
    spec:
      containers:
      - name: cassandra
        image: cassandra
        ports:
        - containerPort: 9042
        volumeMounts:
        - name: cassandra-data
          mountPath: /var/lib/cassandra
  volumeClaimTemplates:
  - metadata:
      name: cassandra-data
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```
Ce fichier YAML crée un StatefulSet avec trois répliques, chacune exécutant l'image Docker "cassandra:latest". Chaque réplique est nommée "cassandra-0", "cassandra-1" et "cassandra-2" respectivement. Chaque réplique a également un volume persistant monté à "/var/lib/cassandra".

Le volume persistant est défini à l'aide d'un "volumeClaimTemplates", qui crée un nouveau volume persistant pour chaque réplique.

Ce fichier YAML garantit que chaque réplique a un nom unique et des données persistantes.


