1.	Création d’un StatefulSet 
-	Utilisant l’apiVersion: apps/v1
-	avec 1 réplicas de Pod de l’image twalter/openshift-nginx
-	utilisant du stockage persistant

```shell
  volumeClaimTemplates:
  - metadata:
      name: hostpath-volume-claim-sts
    spec:
      accessModes:
        - ReadWriteOnce
      resources:
        requests:
          storage: 10Mi
```

-	le volume est monté dans le directory /usr/share/nginx/html du pod		
```shell
 spec:
   …
      containers:                                
      - name: nginx
	     …	
        volumeMounts:
        - name: hostpath-volume-claim-sts
       	 mountPath: /usr/share/nginx/html
```
2.	Quelle est le nom du Pod ? 
3.	Dans le pod du StatefulSet, écrire :
-	echo 'Hello world!'  > /usr/share/nginx/html/index.html
4.	Vérifier avec une commade curl que le serveur web renvoie bien Hello world ! à la place du message par défaut de nginx.
5.	Décommissionner le statefulset
