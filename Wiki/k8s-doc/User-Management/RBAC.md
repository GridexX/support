Les RBAC (Role-Based Access Control) sont une fonctionnalité de Kubernetes qui permettent de contrôler l'accès aux ressources de Kubernetes en fonction des rôles et des autorisations attribuées à chaque utilisateur ou groupe d'utilisateurs.

Voici un exemple de configuration RBAC simple pour Kubernetes :

Créez un fichier de configuration "role.yaml" contenant les informations sur le rôle que vous voulez créer, par exemple :

```shell 
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: default
  name: pod-reader
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "watch", "list"]
```

Ceci crée un rôle nommé "pod-reader" qui autorise les utilisateurs à obtenir, surveiller et lister les pods dans le namespace "default".

Créez un fichier de configuration "role-binding.yaml" pour lier le rôle créé à un utilisateur ou un groupe, par exemple :

```shell
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: read-pods
  namespace: default
subjects:
- kind: User
  name: alice
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

Ceci crée un lien entre le rôle "pod-reader" et l'utilisateur "alice", ce qui permet à "alice" d'accéder aux ressources autorisées par le rôle.

Appliquez les fichiers de configuration à Kubernetes en utilisant la commande kubectl apply, par exemple :

```shell
kubectl apply -f role.yaml
kubectl apply -f role-binding.yaml
```

Ceci applique les configurations de rôle et de liaison de rôle à Kubernetes.

Maintenant, si "alice" essaie d'accéder à une ressource Kubernetes en dehors des autorisations accordées par le rôle "pod-reader", elle recevra une erreur de permission refusée.

Cet exemple montre comment les RBAC Kubernetes peuvent être utilisés pour limiter l'accès aux ressources de Kubernetes en fonction des rôles et des autorisations accordées à chaque utilisateur ou groupe d'utilisateurs.
