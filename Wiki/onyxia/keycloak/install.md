# Install keycloak

```sh
helm repo add codecentric https://codecentric.github.io/helm-charts

helm install keycloak codecentric/keycloak -f keycloak-values.yaml
```

Après l'installation, il faut suivre la doc officiel pour l'intégrer avec onyxia. 

https://docs.onyxia.sh/
