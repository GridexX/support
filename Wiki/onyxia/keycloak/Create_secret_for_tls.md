
## Step 1: Generate a CA private key

```sh
OpenSSL genrsa -out ca.key 2048
```
## OpenSSL genrsa -out ca.key 2048

```sh
openssl req -x509 \
  -new -nodes  \
  -days 365 \
  -key ca.key \
  -out ca.crt \
  -subj "/CN=key.195.220.237.105.nip.io"
```

## Step 3: Now, create the tls secret using the kubectl command or using the yaml definition.

```sh
$ kubectl create secret tls my-tls-secret \
--key ca.key \
--cert ca.crt
secret "my-tls-secret" created
```
