
# Install helm

```sh
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh

```
## Use nip.io for DNS

https://nip.io/

exemple d'utilisation

onyxia.@YOUR_IP.nip.io

PS: il faut créer un groupe de sécurité pour ouvrir le traffic https / http 

## Install onyxia

```sh
helm repo add inseefrlab https://inseefrlab.github.io/helm-charts
helm install onyxia inseefrlab/onyxia \
    --set ingress.enabled=true \
    --set ingress.hosts[0].host=onyxia.@YOUR_IP.nip.io

```

