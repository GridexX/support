# OpenStack-Client (CLI)
Voici un petit tutoriel afin d'installé OpenStack-Client (CLI) pour le CROcc (Cloud Régional Occitanie).

sources :
- https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
- https://pypi.org/project/python-openstackclient/


### A - Installation
Il existe beaucoup de façon d'installer Ansible. Nous allons ici aborder la façon avec pip et un environnement virtuel python. Pour cela, il faut que python soit installé (de préférence python3), ainsi que le package venv de python :

        $ sudo apt -y install python3 python3-venv


CRéez ensuite un environnement virtuel :

       $ python3 -m venv ansible-crocc


Activez cette environnement virtuel :

:warning:l'activation de l'environnement virtuel doit se faire à chaque nouvelle connexion

        $ source ansible-crocc/bin/activate

l'activation de l'environnement virtuel se symbolise par les parenthèse en début de prompt *__"(ansible-crocc)"__*. 

![Activation](./images/OpenStack-Client/activate.png "Activation")


Installez maintenant OpenStack-Client (CLI) par la commande pip:

:warning: l'installation d'OpenStack-Client (CLI) est indépendante de Ansible (§A)

        $ python3 -m pip install python-openstackclient


Vérifiez l'installation d'OpenStack-Client (CLI) :

        $ openstack configuration show

![Vérification](./images/OpenStack-Client/verification.png "Vérification")

## B - Vérification 
Avant l'exécution de notre premier playbook, nous allons sourcer le fichier d'identification d'application créer dans le *[QuickStart §A.1.2](https://forgemia.inra.fr/crocc-users/support/-/blob/main/Wiki/Quick_Start.md#a12-ajout-dune-clefs-publique-ssh)*.

        $ source ./monidentifiantapi.sh


On peut vérifier le fonctionnement de OpenStack-Client (CLI) par la commande, en vérifiant la présence des champ 'application_credential_id', 'application_credential_secret' et 'auth_url' correctement paramétré :

        $ openstack configuration show

![Connecté](./images/OpenStack-Client/connected.png "Connecté")

Ou avec 

        $ openstack flavor list

![Connecté vue gabarits](./images/OpenStack-Client/connected_flavor.png "Connecté vue gabarits")