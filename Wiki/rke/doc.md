# Prérequis 
name	        ip	
rancher1	195.220.237.105	
rancher2	192.168.10.91
rancher3	192.168.10.98	
## Ubuntu

```shell
# Ubuntu instructions 
# stop the software firewall
systemctl disable --now ufw

# get updates, install nfs, and apply
apt update
apt install nfs-common -y  
apt upgrade -y

# clean up
apt autoremove -y

```
# RKE2 SERVER

```shell
# On rancher1
curl -sfL https://get.rke2.io | INSTALL_RKE2_CHANNEL=v1.24 INSTALL_RKE2_TYPE=server sh - 

# start and enable for restarts - 
systemctl enable --now rke2-server.service

chmod 777 /etc/rancher/rke2/rke2.yaml

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

export KUBECONFIG=/etc/rancher/rke2/rke2.yaml 

kubectl get node

```
### Get the token

```shell
cat /var/lib/rancher/rke2/server/node-token
```

# RKE2 Agent install

```shell
export RANCHER1_IP=195.220.237.105
export TOKEN=XXXXXXXXXXXXXXXXX

curl -sfL https://get.rke2.io | INSTALL_RKE2_CHANNEL=v1.24 INSTALL_RKE2_TYPE=agent sh - 
mkdir -p /etc/rancher/rke2/
echo "server: https://$RANCHER1_IP:9345" > /etc/rancher/rke2/config.yaml
echo "token: $TOKEN" >> /etc/rancher/rke2/config.yaml
systemctl enable --now rke2-agent.service
```
