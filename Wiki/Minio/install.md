

# Install the helm chart

```sh
helm repo add minio https://helm.min.io/ --insecure-skip-tls-verify  

kubectl create ns minio  

helm install minio minio/minio --namespace=minio --version 8.0.10  --set persistence.size=5Gi -f values.yaml

```


# Apply ingress

```sh
kubectl apply -f ingress-minio.yaml
```

## PS: Pourquoi on a pas utilisé l'ingress du helm chart 

L'ingress fourni par le helm chart utilise une version d'api qui n'est pas compatible avec l'RKE.
